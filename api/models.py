# https://docs.djangoproject.com/en/3.0/ref/models/fields/#choices
from django.db import models
"""
**Manufacturer**
    *   name: str
    *   website: url
*   **ShoeType**
    *   style: str
*   **ShoeColor**
    *   color_name: str (ROYGBIV + white / black)
*   **Shoe**
    *   size: int
    *   brand name: str
    *   manufacturer: FK (<span>Foreign Key</span>)
    *   color: FK
    *   material: str
    *   shoe_type: FK
    *   fasten_type: str
"""


# Create your models here.
class Manufacturer(models.Model):
    name = models.CharField(max_length=200)
    website = models.URLField(max_length=500)

    def __str__(self):
        return self.name


class ShoeType(models.Model):
    style = models.CharField(max_length=100)

    def __str__(self):
        return self.style


class ShoeColor(models.Model):
    RED = 'RED'
    ORANGE = 'ORANGE'
    YELLOW = 'YELLOW'
    GREEN = 'GREEN'
    BLUE = 'BLUE'
    INDIGO = 'INDIGO'
    VIOLET = 'VIOLET'
    WHITE = 'WHITE'
    BLACK = 'BLACK'
    COLOR_CHOICES = [
        (RED, 'Red'),
        (ORANGE, 'Orange'),
        (YELLOW, 'Yellow'),
        (GREEN, 'Green'),
        (BLUE, 'Blue'),
        (INDIGO, 'Indigo'),
        (VIOLET, 'Violet'),
        (WHITE, 'White'),
        (BLACK, 'Black')
    ]
    color_name = models.CharField(max_length=6, choices=COLOR_CHOICES, default=BLACK)

    def __str__(self):
        return self.color_name


class Shoe(models.Model):
    size = models.IntegerField()
    brand_name = models.CharField(max_length=200)
    manufacturer = models.ForeignKey(Manufacturer, on_delete=models.CASCADE)
    color = models.ForeignKey(ShoeColor, on_delete=models.CASCADE)
    material = models.CharField(max_length=100)
    shoe_type = models.ForeignKey(ShoeType, on_delete=models.CASCADE)
    fasten_type = models.CharField(max_length=100)

    def __str__(self):
        return self.brand_name
