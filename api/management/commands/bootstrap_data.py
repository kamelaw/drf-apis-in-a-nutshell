# https://docs.djangoproject.com/en/3.0/howto/custom-management-commands/

from django.core.management.base import BaseCommand
from api.models import ShoeType, ShoeColor


class Command(BaseCommand):
    help = 'Submits data for the shoe color and style'

    def handle(self, *args, **options):
        shoe_colors = ["Red", "Orange", "Yellow", "Green", "Blue", "Indigo", "Violet", "White", "Black"]
        shoe_styles = ["sneaker", "boot", "sandal", "dress", "other"]

        for color in shoe_colors:
            ShoeColor.objects.create(
                color_name=color
            )
        for style in shoe_styles:
            ShoeType.objects.create(
                style=style
            )

